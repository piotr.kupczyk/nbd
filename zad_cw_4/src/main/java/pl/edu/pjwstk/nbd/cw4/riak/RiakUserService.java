package pl.edu.pjwstk.nbd.cw4.riak;

import com.basho.riak.client.api.RiakClient;
import com.basho.riak.client.api.cap.Quorum;
import com.basho.riak.client.api.cap.UnresolvedConflictException;
import com.basho.riak.client.api.commands.kv.DeleteValue;
import com.basho.riak.client.api.commands.kv.FetchValue;
import com.basho.riak.client.api.commands.kv.StoreValue;
import com.basho.riak.client.api.commands.kv.UpdateValue;
import com.basho.riak.client.core.query.Location;
import com.basho.riak.client.core.query.Namespace;
import com.basho.riak.client.core.query.RiakObject;
import com.basho.riak.client.core.util.BinaryValue;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class RiakUserService {

    private RiakClient riakClient;
    private Namespace defaultBucket;
    private ObjectMapper objectMapper = new ObjectMapper();

    public RiakUserService(RiakClient riakClient, String defaultBucketName) {
        this.riakClient = riakClient;
        defaultBucket = new Namespace(defaultBucketName);
    }

    public void saveUser(String key, User user) throws ExecutionException, InterruptedException {
        Location userKey = new Location(defaultBucket, key);

        StoreValue storeValue =
                new StoreValue.Builder(user)
                        .withLocation(userKey)
                        .withOption(StoreValue.Option.W, new Quorum(3))
                        .withOption(StoreValue.Option.RETURN_BODY, true)
                        .build();

        riakClient.execute(storeValue);
    }

    public User getUserByKey(String key) throws ExecutionException, InterruptedException, IOException {
        Location userKey = new Location(defaultBucket, key);

        FetchValue fetchValueCommand = new FetchValue.Builder(userKey).build();

        return Optional.of(riakClient.execute(fetchValueCommand))
                .map(response -> {
                    try {
                        return response.getValue(RiakObject.class);
                    } catch (UnresolvedConflictException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .map(RiakObject::getValue)
                .map(binaryValue -> {
                    try {
                        return objectMapper.readValue(new String(binaryValue.getValue()), User.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).orElse(null);
    }

    public void updateUser(String key, User newUser) throws ExecutionException, InterruptedException {
        Location userKey = new Location(defaultBucket, key);
        UpdateValue updateOp =
                new UpdateValue.Builder(userKey)
                        .withFetchOption(FetchValue.Option.DELETED_VCLOCK, true)
                        .withUpdate(new UserClobberUpdate(newUser))
                        .build();

        riakClient.execute(updateOp);
    }

    public void deleteUserByKey(String key) throws ExecutionException, InterruptedException {
        Location userKey = new Location(defaultBucket, key);
        DeleteValue delete = new DeleteValue.Builder(userKey).build();

        riakClient.execute(delete);
    }
}
