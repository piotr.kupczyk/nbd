package pl.edu.pjwstk.nbd.cw4.riak;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

    @JsonProperty("name") String name;
    @JsonProperty("nationality") String nationality;
    @JsonProperty("age") Integer age;
    @JsonProperty("student") Boolean student;

    public User() { }

    public User(String name, String nationality, Integer age, Boolean student) {
        this.name = name;
        this.nationality = nationality;
        this.age = age;
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!name.equals(user.name)) return false;
        if (!nationality.equals(user.nationality)) return false;
        if (!age.equals(user.age)) return false;
        return student.equals(user.student);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + nationality.hashCode();
        result = 31 * result + age.hashCode();
        result = 31 * result + student.hashCode();
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getStudent() {
        return student;
    }

    public void setStudent(Boolean student) {
        this.student = student;
    }
}
