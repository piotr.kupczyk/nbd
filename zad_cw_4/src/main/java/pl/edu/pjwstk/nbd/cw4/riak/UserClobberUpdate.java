package pl.edu.pjwstk.nbd.cw4.riak;

import com.basho.riak.client.api.commands.kv.UpdateValue;

public class UserClobberUpdate extends UpdateValue.Update<User>{
    User newUser;

    public UserClobberUpdate(User newUser) {
        this.newUser = newUser;
    }

    @Override
    public User apply(User original) {
        return newUser;
    }
}
