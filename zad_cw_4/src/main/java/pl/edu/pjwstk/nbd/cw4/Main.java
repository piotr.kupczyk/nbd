package pl.edu.pjwstk.nbd.cw4;

import com.basho.riak.client.api.RiakClient;
import org.apache.log4j.BasicConfigurator;
import pl.edu.pjwstk.nbd.cw4.riak.RiakUserService;
import pl.edu.pjwstk.nbd.cw4.riak.User;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) {
        RiakClient riakClient = null;
        try {
            BasicConfigurator.configure();

            User user = new User("Piotr Kupczyk", "PL", 23, true);
            String USER_KEY = "Piotr";
            riakClient = RiakClient.newClient(8087, "localhost");
            RiakUserService userRiakUserService = new RiakUserService(riakClient, "s22001");

            userRiakUserService.saveUser(USER_KEY, user);
            System.out.println((userRiakUserService.getUserByKey(USER_KEY)));
            userRiakUserService.updateUser(USER_KEY, new User(user.getName(), user.getNationality(), user.getAge(), false));
            System.out.println((userRiakUserService.getUserByKey(USER_KEY)));
            userRiakUserService.deleteUserByKey(USER_KEY);
            System.out.println((userRiakUserService.getUserByKey(USER_KEY)));
        } catch (InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
        } finally {
            riakClient.getRiakCluster().shutdown();
        }

    }
}
