#!/usr/bin/env bash

# 1.1. CREATE BUCKET
curl -i -o result-1.1.txt -XPUT -H 'Content-Type: application/json' -d '{ "props": { "n_val": 5 } }' http://localhost:8098//buckets/s22001/props

# 1.2. ADD USER TO BUCKET
curl -i -o result-1.2.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Piotr

# 1.3. ADD USER TO BUCKET
curl -i -o result-1.3.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Adam Kowalski", "nationality": "PL", "age": 25, "isStudent": false }' http://localhost:8098/buckets/s22001/keys/Adam

# 1.4. ADD USER TO BUCKET
curl -i -o result-1.4.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Kacper Nowak", "nationality": "PL", "age": 29, "isStudent": false }' http://localhost:8098/buckets/s22001/keys/Kacper

# 1.5. ADD USER TO BUCKET
curl -i -o result-1.5.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "John Nowak", "nationality": "USA", "age": 21, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/John

# 1.6. ADD USER TO BUCKET
curl -i -o result-1.6.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Lars Brune", "nationality": "GER", "age": 24, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Lars

# 2.	Pobierz z bazy jedną z dodanych przez Ciebie wartości.
curl -i -o result-2.txt -XGET http://localhost:8098/buckets/s22001/keys/Piotr

# 3.	Zmodyfikuj jedną z wartości – dodając dodatkowe pole do dokumentu.
curl -i -o result-3.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true, "hasStudentPrivilages": true }' http://localhost:8098/buckets/s22001/keys/Piotr

# 4.	Zmodyfikuj jedną z wartości – usuwając jedną pole z wybranego dokumentu.
curl -i -o result-4.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Piotr

# 5.	Zmodyfikuj jedną z wartości – zmieniając wartość jednego z pól.
curl -i -o result-5.txt -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 24, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Piotr

# 6.	Usuń jeden z dokumentów z bazy.
curl -i -o result-6.txt -XDELETE http://localhost:8098/buckets/s22001/keys/Piotr

# 7.	Spróbuj pobrać z bazy wartość, która nie istnieje w tej bazie.
curl -i -o result-7.txt -XGET http://localhost:8098/buckets/s22001/keys/Piotr

# 8.	Dodaj do bazy 1 dokument json (zawierający 1 pole), ale nie specyfikuj klucza.
curl -i -o result-8.txt -XPOST -H 'Content-Type: application/json' -d '{ "sampleNumber": 12345 }' http://localhost:8098/buckets/s22001/keys

## 9.
curl -i -o result-9.txt -XGET http://localhost:8098/buckets/s22001/keys/IYXXnYVw4nLsQ68HrwYfZFIcLQt
#
## 10.
curl -i -o result-10.txt -XDELETE http://localhost:8098/buckets/s22001/keys/IYXXnYVw4nLsQ68HrwYfZFIcLQt

exit 0;