curl -i -XPUT -H 'Content-Type: application/json' -d '{ "props": { "n_val": 5 } }' http://localhost:8098//buckets/s22001/props
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Piotr
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Adam Kowalski", "nationality": "PL", "age": 25, "isStudent": false }' http://localhost:8098/buckets/s22001/keys/Adam
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Kacper Nowak", "nationality": "PL", "age": 29, "isStudent": false }' http://localhost:8098/buckets/s22001/keys/Kacper
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "John Nowak", "nationality": "USA", "age": 21, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/John
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Lars Brune", "nationality": "GER", "age": 24, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Lars
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

# 2.	Pobierz z bazy jedną z dodanych przez Ciebie wartości.

curl -i -XGET http://localhost:8098/buckets/s22001/keys/Piotr

HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8tgFmdrvyhZQYGNQFM5gSOfNYGTSPsd/gywIA
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/s22001>; rel="up"
Last-Modified: Sun, 12 Apr 2020 17:04:41 GMT
ETag: "6oLQlxAmjWXhZGKMxw2CcR"
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 78

# 3.	Zmodyfikuj jedną z wartości – dodając dodatkowe pole do dokumentu.

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true, "hasStudentPrivilages": true }' http://localhost:8098/buckets/s22001/keys/Piotr

{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true }HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

# 4.	Zmodyfikuj jedną z wartości – usuwając jedną pole z wybranego dokumentu.

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 23, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Piotr

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

# 5.	Zmodyfikuj jedną z wartości – zmieniając wartość jednego z pól.

curl -i -XPUT -H 'Content-Type: application/json' -d '{ "name": "Piotr Kupczyk", "nationality": "PL", "age": 24, "isStudent": true }' http://localhost:8098/buckets/s22001/keys/Piotr

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

# 6.	Usuń jeden z dokumentów z bazy.

curl -i -XDELETE http://localhost:8098/buckets/s22001/keys/Piotr

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

# 7.	Spróbuj pobrać z bazy wartość, która nie istnieje w tej bazie.

curl -i -XGET http://localhost:8098/buckets/s22001/keys/Piotr

HTTP/1.1 404 Object Not Found
X-Riak-Vclock: a85hYGBgzGDKBVI8tgFmdrvyhZQYGNQFM5gSefNYGTSPsd/gywIA
X-Riak-Deleted: true
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: text/plain
Content-Length: 10


# 8.	Dodaj do bazy 1 dokument json (zawierający 1 pole), ale nie specyfikuj klucza.

curl -i -XPOST -H 'Content-Type: application/json' -d '{ "sampleNumber": 12345 }' http://localhost:8098/buckets/s22001/keysnot found

HTTP/1.1 201 Created
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Location: /buckets/s22001/keys/Hlb3LNoiopXFmjWtmj8yOiy3JJX
Date: Sun, 12 Apr 2020 17:04:41 GMT
Content-Type: application/json
Content-Length: 0

# 9.	Pobierz z bazy element z zadania 8.

curl -i -XGET http://localhost:8098/buckets/s22001/keys/Hlb3LNoiopXFmjWtmj8yOiy3JJX

HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8tgFmdruKc2YwMKgLZTAlMuaxMhgdZ7/BlwUA
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/s22001>; rel="up"
Last-Modified: Sun, 12 Apr 2020 17:09:06 GMT
ETag: "drPX2yLOgGxKJBZwCI34c"
Date: Sun, 12 Apr 2020 17:09:27 GMT
Content-Type: application/json
Content-Length: 25

{ "sampleNumber": 12345 }%

# 10.	Usuń z bazy element z zadania 8.

curl -i -XDELETE http://localhost:8098/buckets/s22001/keys/Hlb3LNoiopXFmjWtmj8yOiy3JJX

HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Sun, 12 Apr 2020 17:07:46 GMT
Content-Type: application/json
Content-Length: 0

