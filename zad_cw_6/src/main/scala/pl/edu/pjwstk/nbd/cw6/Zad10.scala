package pl.edu.pjwstk.nbd.cw6

object Zad10 {

  def toAbsWhenInRange(list: List[Double]): List[Double] =
    list.filter( number => number >= -5 && number <= 12)
    .map( _ abs )

}
