package pl.edu.pjwstk.nbd.cw6

object Zad5 {
  def patternMatching(string: String): String =
    string match {
      case "MONDAY" | "TUESDAY" | "WEDNESDAY" | "THURSDAY" | "FRIDAY" => "Praca"
      case "SATURDAY" | "SUNDAY" => "Weekend"
      case _ => "Nie ma takiego dnia"
    }
}
