package pl.edu.pjwstk.nbd.cw6

object Zad4 {
  val products = Map("T-SHIRT" -> 20.0, "JACKET" -> 100.0, "SOCKS" -> 8.0)

  def demonstrateOption(input: Map[String, Double], key: String, default: Double): Double = {
    input.get(key) match {
      case Some(x) => x
      case None => default
    }
  }
}
