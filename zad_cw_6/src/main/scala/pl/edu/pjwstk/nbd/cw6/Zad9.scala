package pl.edu.pjwstk.nbd.cw6

object Zad9 {

  def addOne(list: List[Int]): List[Int] =
    list.map(_ + 1)
}
