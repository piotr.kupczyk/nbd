package pl.edu.pjwstk.nbd.cw6

// 6.	Zdefiniuj klasę KontoBankowe z metodami wplata i wyplata oraz własnością stanKonta - własność ma być tylko do odczytu. Klasa powinna udostępniać konstruktor przyjmujący początkowy stan konta oraz drugi, ustawiający początkowy stan konta na 0.

class BankAccount(private var _balance: Double = 0.0) {
  def balance: Double = _balance

  def deposit(amount: Double): Unit = _balance += amount

  def withdraw(amount: Double): Unit = _balance -= amount

}