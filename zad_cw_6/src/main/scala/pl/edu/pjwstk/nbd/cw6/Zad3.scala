package pl.edu.pjwstk.nbd.cw6

object Zad3 {

  def printTriple(triple: (String, Int, Double)): Unit = {
    println(s"STRING: ${triple._1}")
    println(s"INT: ${triple._2}")
    println(s"DOUBLE: ${triple._3}")
  }

}
