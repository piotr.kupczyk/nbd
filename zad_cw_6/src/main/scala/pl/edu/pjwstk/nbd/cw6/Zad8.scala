package pl.edu.pjwstk.nbd.cw6

object Zad8 {

  def removeZeros(list: List[Int]): List[Int] =
    list.filter(_ != 0)

}
