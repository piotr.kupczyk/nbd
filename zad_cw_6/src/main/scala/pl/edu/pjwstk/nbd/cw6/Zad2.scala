package pl.edu.pjwstk.nbd.cw6

object Zad2 {

  val products = Map("T-SHIRT" -> 20.0, "JACKET" -> 100.0, "SOCKS" -> 8.0)

  val productsAfterDiscount: Map[String, Double] = products.map(t => (t._1, t._2*0.9))

}
