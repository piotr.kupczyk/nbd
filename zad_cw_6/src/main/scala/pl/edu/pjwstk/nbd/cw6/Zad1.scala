package pl.edu.pjwstk.nbd.cw6

object Zad1 {
  // 1a
  def printFor(daysOfWeek: List[String]): Unit = {
    for (i <- daysOfWeek.indices) {
      println(daysOfWeek(i))
    }
  }

  // 1b
  def printStartingWithP(daysOfWeek: List[String]): Unit = {
    for (i <- daysOfWeek.indices) {
      if (daysOfWeek(i).startsWith("P"))
        println(daysOfWeek(i))
    }
  }

  // 1c
  def printForEach(daysOfWeek: List[String]): Unit = {
    daysOfWeek.foreach(day =>
      println(day)
    )
  }

  // 1d
  def printWhile(daysOfWeek: List[String]): Unit = {
    var i = 0
    while (daysOfWeek.indices.contains(i)) {
      println(daysOfWeek(i))
      i += 1
    }

  }

  // 1e
  def printRecursive(daysOfWeek: List[String]): Unit = {
    if (daysOfWeek.nonEmpty) {
      println(daysOfWeek.head)
      printRecursive(daysOfWeek.tail)
    }
  }

  // 1f
  def printRecursiveReversed(daysOfWeek: List[String]): Unit = {
    if (daysOfWeek.nonEmpty) {
      printRecursiveReversed(daysOfWeek.tail)
      println(daysOfWeek.head)
    }
  }

  // 1g
  def printFold(daysOfWeek: List[String]): Unit = {
    println("==FOLD RIGHT==")
    daysOfWeek.foldRight()((day, _) => println(day))
    println("==FOLD LEFT==")
    daysOfWeek.foldLeft()((_, day) => println(day))
  }

  // 1h
  def printFoldStartingWithP(daysOfWeek: List[String]): Unit = {
    daysOfWeek.foldLeft()((_, day) => {
      if (day.startsWith("P"))
        println(day)
    })
  }
}
