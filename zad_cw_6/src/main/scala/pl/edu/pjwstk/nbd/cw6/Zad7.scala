package pl.edu.pjwstk.nbd.cw6

/*
7.	Zdefiniuj klasę Osoba z własnościami imie i nazwisko. Stwórz kilka instancji tej klasy. Zdefiniuj funkcję,
która przyjmuje obiekt klasy osoba i przy pomocy Pattern Matching wybiera i zwraca napis zawierający przywitanie danej osoby.
Zdefiniuj 2-3 różne przywitania dla konkretnych osób (z określonym imionami lub nazwiskami) oraz jedno domyślne.
 */

case class Person(name: String, surname: String)

object Zad7 {
  val firstPerson: Person = Person("Piotr", "Kupczyk")
  val secondPerson: Person = Person("Piotr", "Kupczykk")
  val thirdPerson: Person = Person("Piotr", "Kupczykkk")

  def sayHi(person: Person): String =
    person match {
      case Person("Piotr", "Kupczyk") => "Hi"
      case Person("Piotr", "Kupczykk") => "Hi Hi"
      case Person("Piotr", "Kupczykkk") => "Hi Hi Hi"
      case _ => "Cannot say hi..."
    }
}