# NBD Piotr Kupczyk

## Zad cw_2
Rozwiązania znajdują się w folderze `zad_cw_2/results/`.

`zapytanie_X.js` odpowiada zapytaniu na zadanie X, gdzie x odpoowiada numerowi zadania.
 
`wynik_X.js` odpowiada wynikowi `zapytania_X.js`, gdzie x odpoowiada numerowi zadania.

Dla ułatwienia obliczeń numerycznych `zapytanie_1_1.js` dodaje numeryczne wartości pól `weight`, `height` jako `weight_number` i `height_number` .
 

## Zad cw_3
Rozwiązania znajdują się w folderze `zad_cw_3/results/`

Zapytania znajdują się w pliku `zad_cw_3/queries.txt`

Zapytanie `3.0` generuje dodatkową relację `THE_CHEAPEST_CONNECTION`.

Zapytania `1.1`, `2.1`, `3.1` polegały jedynie na zaimportowaniu danych, więc zakładam, że odpowedzi do nich nie były potrzebne.
Jeżeli jednak by były, to proszę o kontakt na email studencki `s22001@pjwstk.edu.pl`.
## Zad cw_4
Polecenia curl znajdują się w pliku `zad_cw_4/requests.txt`

Rozwiązania znajdują się w folderze `zad_cw_4/`.

Odpowiedzi serwera 1-10 znajdują się w plikach result-x.txt, gdzie x odpoowiada numerowi zadania.

Zadanie 11 napisane zostało w języku Java i znajduje się w folderze `zad_cw_4/src/main/java/pl/edu/pjwstk/nbd/cw4/`.

## Zad cw_6
Rozwiązania znajdują się w folderze `zad_cw_6/src/main/java/pl/edu/pjwstk/nbd/cw6`.

Rozwiązania konkretnych zadań znajdują się w plikach `ZadX`, gdzie `X` odpoowiada numerowi zadania. 
W pliku `Main.scala` znajdują się przykładowe wywołania poszczególnych zadań.
