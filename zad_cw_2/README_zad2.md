### ZAD2 - Bazy dokumentowe

Zadanie wykonane zostało za pomocą bazy postawionej na dokerze, natomiast zapytania powinny bez problemu działać na dowolnej instacji MongoDB.
Wszystkie zapytania/rozwiązania znajdują się w katalogu `results`.
Dla ułatwienia obliczeń numerycznych `zapytanie_1_1.js` dodaje numeryczne wartości pól `weight`, `height` jako `weight_number` i `height_number` .
Testowane na `MongoDB:4.2.6` 


#### Jak uruchomić na dokerze?

W katalogu `zad_cw_2` komenda `docker-compose up`. Gdy baza się uruchomi, `docker exec -ti zad_cw_2_mongo_1 bash`, a nastepnie w katalogu `/results` uruchomić skrypt `./getResult.sh`.


### Poniżej polecenia i zapytania:


### 1. Dodanie do bazy kolumny weight_number i height_number

```
db.people.find({weight: {$exists: true}, height: {$exists: true}}).forEach(function (obj) {
    obj.weight_number = parseFloat(obj.weight)
    obj.height_number = parseFloat(obj.height)
    db.people.save(obj);
})
```


### 2.	Jedna osoba znajdująca się w bazie;
```
db.people.findOne()
```

### 3.	Jedna kobieta narodowości chińskiej;
```
db.people.findOne({sex: "Female"})
```

### 4.	Lista mężczyzn narodowości niemieckiej;
```
db.people.find({nationality: "Germany"})
```

### 5.	Lista wszystkich osób znajdujących się w bazie o wadze z przedziału <68, 71.5);
```
db.people.find({weight: {$gte: "68.0", $lt: "71.5"}})
```

### 6.	Lista imion i nazwisk wszystkich osób znajdujących się w bazie oraz miast, w których mieszkają, ale tylko dla osób urodzonych w XXI wieku;
```
db.people.find({birth_date: {$gte: "2000-01-01"}}, {_id: 0, first_name: 1, last_name: 1, "location.city": 1})
```

### 7.	Dodaj siebie do bazy, zgodnie z formatem danych użytych dla innych osób (dane dotyczące karty kredytowej, adresu zamieszkania i wagi mogą być fikcyjne);
```
db.people.insertOne(
       {
           "sex": "Male",
           "first_name": "Piotr",
           "last_name": "Kupczyk",
           "job": "Software Developer",
           "email": "piotr.kupczyk@pjwstk.edu.pl",
           "location": {"city": "Wroclaw", "address": {"streetname": "Some street", "streetnumber": "100"}},
           "description": "vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris",
           "height": "200.00",
           "weight": "100.00",
           "height_number": 200.00,
           "weight_number": 100.00,
           "birth_date": "1997-08-02T02:55:03Z",
           "nationality": "Poland",
           "credit": [{"type": "switch", "number": "6759888939100098699", "currency": "COP", "balance": "5117.06"}]
       }
   )
```

### 8.	Usuń z bazy osoby o wzroście przekraczającym 190;
```
db.people.deleteMany( {
    height_number: { $gt: 190.00 }
} )
```

### 9.	 Zastąp nazwę miasta „Moscow” przez „Moskwa” u wszystkich osób w bazie;
```
db.people.updateMany(
    { "location.city": "Moscow" },
    { $set: { "location.city": "Moskwa" } }
    )
```

### 10.	Dodaj do wszystkich osób o imieniu Antonio własność „hobby” o wartości „pingpong”;
```
db.people.updateMany(
    { first_name: "Antonio" },
    { $set: { hobby: "pingpong" } }
    )
```


### 11.	Usuń u wszystkich osób o zawodzie „Editor” własność „email”.
```
db.people.updateMany(
    { job: "Editor" },
    { $unset: { email: "" } }
    )
```

### 1.	Średnią wagę i wzrost osób w bazie z podziałem na płeć (tzn. osobno mężczyzn, osobno kobiet);
```
db.people.aggregate([
    {$project: {_id: 0, sex: 1, weight_number: 1, height_number: 1}},
    {
        $group: {
            _id: "$sex",
            avgHeight: {$avg: "$height_number"},
            avgWeight: {$avg: "$weight_number"}
        }
    }
])
```
```
db.people.mapReduce(
    function () {
        if (this.count === undefined)
            emit(this.sex, {height_number: this.height_number, weight_number: this.weight_number, count: 1})
        else
            emit(this.sex, {height_number: this.height_number, weight_number: this.weight_number, count: this.count})
    },
    function (key, values) {
        const reducedValue = {height_number: 0, weight_number: 0, count: 0}
        for (let i = 0; i < values.length; i++) {
            reducedValue.height_number += values[i].height_number
            reducedValue.weight_number += values[i].weight_number
            reducedValue.count += values[i].count
        }

        return reducedValue;
    },
    {
        out: { inline: 1 },
        finalize: function (key, reducedValue) {
            reducedValue.avgHeight = reducedValue.height_number/reducedValue.count;
            reducedValue.avgWeight = reducedValue.weight_number/reducedValue.count;
            return reducedValue;
        }
    }
    ).find()
```

### 2.	Łączną ilość środków pozostałych na kartach kredytowych osób w bazie, w podziale na waluty;
```
db.people.aggregate([
    {$project: {credit: 1}},
    {$unwind: "$credit"},
    {
        $group: {
            _id: "$credit.currency",
            totalBalance: { $sum: { $toDouble: "$credit.balance" } }
        }
    }
])
```

```
db.people.mapReduce(
    function () {
        this.credit.forEach(credit => {
            emit(credit.currency, credit.balance)
        })
    },
    function (key, values) {
        return Array.sum(values.map(value => parseFloat(value)))
    },
    {
        out: { inline: 1}
    }
    ).find()
```

### 3.	Listę unikalnych zawodów;
```
db.people.aggregate([
    {$project: {_id: 0, job: 1}},
    {$group: {_id: "$job"}}
])
```

```
db.people.mapReduce(
    function () { emit(this.job, null) },
    function (key, values) { return null },
    {
        out: { inline: 1 },
        finalize: function(key, value) {
            return key;
        }
    }
    ).find()
```

### 4.	Średnie, minimalne i maksymalne BMI (waga/wzrost^2) dla osób w bazie, w podziale na narodowości;
```
db.people.aggregate([
    {
        $project: {
            _id: 0,
            nationality: 1,
            weight_number: 1,
            height_number: 1,
            height_in_meters: {$divide: ["$height_number", 100]}
        }
    },
    {
        $project: {
            _id: 0,
            nationality: 1,
            weight_number: 1,
            height_square: {$pow: ["$height_in_meters", 2]}
        }
    },
    {
        $project: {
            _id: 0,
            nationality: 1,
            weight_number: 1,
            bmi: {$divide: ["$weight_number", "$height_square"]}
        }
    },
    {
        $group: {
            _id: "$nationality",
            avgBMI: {$avg: "$bmi"},
            minBMI: {$min: "$bmi"},
            maxBMI: {$max: "$bmi"}
        }
    }
])
```

```
db.people.mapReduce(
    function () {
        if (this.totalBmi === undefined) {
            const heightInMeters = this.height_number / 100.0
            const heightSqrt = Math.pow(heightInMeters, 2)
            const bmi = this.weight_number / heightSqrt
            emit(this.nationality, { totalBmi: bmi, count: 1, minBMI: bmi, maxBMI: bmi })
        } else {
            emit(this.nationality, { totalBmi: this.totalBmi, count: this.count, minBMI: this.minBMI, maxBMI: this.maxBMI })
        }
    },
    function (key, values) {
        const reducedValue = {totalBmi: 0, count: 0, minBMI: Infinity, maxBMI: 0}
        for (let i = 0; i < values.length; i++) {
            reducedValue.totalBmi += values[i].totalBmi;
            reducedValue.count += values[i].count
            if (values[i].minBMI < reducedValue.minBMI)
                reducedValue.minBMI = values[i].minBMI
            if (values[i].maxBMI > reducedValue.maxBMI)
                reducedValue.maxBMI = values[i].maxBMI
        }

        return reducedValue;
    },
    {
        out: {inline: 1},
        finalize: function (key, reducedValue) {
            return {
                avgBMI: reducedValue.totalBmi/reducedValue.count,
                minBMI: reducedValue.minBMI,
                maxBMI: reducedValue.maxBMI
            }
        }
    }
    ).find()
```

### 5.	Średnia i łączna ilość środków na kartach kredytowych kobiet narodowości polskiej w podziale na waluty.
```
db.people.aggregate([
    {$match: {nationality: "Poland", sex: "Female"}},
    {$project: {credit: 1}},
    {$unwind: "$credit"},
    {
        $group: {
            _id: "$credit.currency",
            totalBalance: { $sum: { $toDouble: "$credit.balance" } },
            avgBalance: { $avg: { $toDouble: "$credit.balance" } }
        }
    }
])
```

```
db.people.mapReduce(
    function () {
        this.credit.forEach(credit => {
            emit(credit.currency, { totalBalance: credit.balance, credits: 1 })
        })
    },
    function (key, values) {
        const balanceAsFloat = values.map(credit => parseFloat(credit.totalBalance));
        return {
            totalBalance: Array.sum(balanceAsFloat),
            credits: balanceAsFloat.length
       }
    },
    {
        out: { inline: 1 },
        query: { nationality: "Poland", sex: "Female" },
        finalize: function (key, value) {
            return {
                avgBalance: value.totalBalance/value.credits,
                totalBalance: value.totalBalance
            }
        }
    }
    ).find()
``` 