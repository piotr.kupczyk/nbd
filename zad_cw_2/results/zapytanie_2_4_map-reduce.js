db.people.mapReduce(
    function () {
        if (this.totalBmi === undefined) {
            const heightInMeters = this.height_number / 100.0
            const heightSqrt = Math.pow(heightInMeters, 2)
            const bmi = this.weight_number / heightSqrt
            emit(this.nationality, { totalBmi: bmi, count: 1, minBMI: bmi, maxBMI: bmi })
        } else {
            emit(this.nationality, { totalBmi: this.totalBmi, count: this.count, minBMI: this.minBMI, maxBMI: this.maxBMI })
        }
    },
    function (key, values) {
        const reducedValue = {totalBmi: 0, count: 0, minBMI: Infinity, maxBMI: 0}
        for (let i = 0; i < values.length; i++) {
            reducedValue.totalBmi += values[i].totalBmi;
            reducedValue.count += values[i].count
            if (values[i].minBMI < reducedValue.minBMI)
                reducedValue.minBMI = values[i].minBMI
            if (values[i].maxBMI > reducedValue.maxBMI)
                reducedValue.maxBMI = values[i].maxBMI
        }

        return reducedValue;
    },
    {
        out: {inline: 1},
        finalize: function (key, reducedValue) {
            return {
                avgBMI: reducedValue.totalBmi/reducedValue.count,
                minBMI: reducedValue.minBMI,
                maxBMI: reducedValue.maxBMI
            }
        }
    }
).find()