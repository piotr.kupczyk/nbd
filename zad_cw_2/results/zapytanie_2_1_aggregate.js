db.people.aggregate([
    {$project: {_id: 0, sex: 1, weight_number: 1, height_number: 1}},
    {
        $group: {
            _id: "$sex",
            avgHeight: {$avg: "$height_number"},
            avgWeight: {$avg: "$weight_number"}
        }
    }
]).toArray()