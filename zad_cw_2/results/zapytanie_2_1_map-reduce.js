db.people.mapReduce(
    function () {
        if (this.count === undefined)
            emit(this.sex, {height_number: this.height_number, weight_number: this.weight_number, count: 1})
        else
            emit(this.sex, {height_number: this.height_number, weight_number: this.weight_number, count: this.count})
    },
    function (key, values) {
        const reducedValue = {height_number: 0, weight_number: 0, count: 0}
        for (let i = 0; i < values.length; i++) {
            reducedValue.height_number += values[i].height_number
            reducedValue.weight_number += values[i].weight_number
            reducedValue.count += values[i].count
        }

        return reducedValue;
    },
    {
        out: { inline: 1 },
        finalize: function (key, reducedValue) {
            return {
                avgHeight: reducedValue.height_number/reducedValue.count,
                avgWeight: reducedValue.weight_number/reducedValue.count
            }
        }
    }
).find()