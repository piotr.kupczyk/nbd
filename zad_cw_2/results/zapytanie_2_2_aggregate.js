db.people.aggregate([
    {$project: {credit: 1}},
    {$unwind: "$credit"},
    {
        $group: {
            _id: "$credit.currency",
            totalBalance: { $sum: { $toDouble: "$credit.balance" } }
        }
    }
]).toArray()