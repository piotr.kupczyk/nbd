db.people.find({weight: {$exists: true}, height: {$exists: true}}).forEach(function (obj) {
    obj.weight_number = parseFloat(obj.weight)
    obj.height_number = parseFloat(obj.height)
    db.people.save(obj);
})