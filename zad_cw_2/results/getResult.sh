#!/usr/bin/env bash
mongo nbd < clearDatabase.js &&
mongoimport --file ./cwiczenia2.json --db nbd --collection people --jsonArray &&
mongo nbd < zapytanie_1_1.js > wyniki_1_1.json &&
mongo nbd < zapytanie_1_2.js > wyniki_1_2.json &&
mongo nbd < zapytanie_1_3.js > wyniki_1_3.json &&
mongo nbd < zapytanie_1_4.js > wyniki_1_4.json &&
mongo nbd < zapytanie_1_5.js > wyniki_1_5.json &&
mongo nbd < zapytanie_1_6.js > wyniki_1_6.json &&
mongo nbd < zapytanie_1_7.js > wyniki_1_7.json &&
mongo nbd < zapytanie_1_8.js > wyniki_1_8.json &&
mongo nbd < zapytanie_1_9.js > wyniki_1_9.json &&
mongo nbd < zapytanie_1_10.js > wyniki_1_10.json &&
mongo nbd < zapytanie_1_11.js > wyniki_1_11.json &&
mongo nbd < zapytanie_2_1_aggregate.js > wyniki_2_1_aggregate.json &&
mongo nbd < zapytanie_2_1_map-reduce.js > wyniki_2_1_map-reduce.json &&
mongo nbd < zapytanie_2_2_aggregate.js > wyniki_2_2_aggregate.json &&
mongo nbd < zapytanie_2_2_map-reduce.js > wyniki_2_2_map-reduce.json &&
mongo nbd < zapytanie_2_3_aggregate.js > wyniki_2_3_aggregate.json &&
mongo nbd < zapytanie_2_3_map-reduce.js > wyniki_2_3_map-reduce.json &&
mongo nbd < zapytanie_2_4_aggregate.js > wyniki_2_4_aggregate.json &&
mongo nbd < zapytanie_2_4_map-reduce.js > wyniki_2_4_map-reduce.json &&
mongo nbd < zapytanie_2_5_aggregate.js > wyniki_2_5_aggregate.json &&
mongo nbd < zapytanie_2_5_map-reduce.js > wyniki_2_5_map-reduce.json
exit 0