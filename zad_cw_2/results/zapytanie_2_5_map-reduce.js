db.people.mapReduce(
    function () {
        this.credit.forEach(credit => {
            emit(credit.currency, { totalBalance: credit.balance, credits: 1 })
        })
    },
    function (key, values) {
        const balanceAsFloat = values.map(credit => parseFloat(credit.totalBalance));
        return {
            totalBalance: Array.sum(balanceAsFloat),
            credits: balanceAsFloat.length
        }
    },
    {
        out: { inline: 1 },
        query: { nationality: "Poland", sex: "Female" },
        finalize: function (key, value) {
            return {
                avgBalance: value.totalBalance/value.credits,
                totalBalance: parseFloat(value.totalBalance)
            }
        }
    }
).find()