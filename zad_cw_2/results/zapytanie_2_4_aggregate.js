db.people.aggregate([
    {
        $project: {
            _id: 0,
            nationality: 1,
            weight_number: 1,
            height_number: 1,
            height_in_meters: {$divide: ["$height_number", 100]}
        }
    },
    {
        $project: {
            _id: 0,
            nationality: 1,
            weight_number: 1,
            height_square: {$pow: ["$height_in_meters", 2]}
        }
    },
    {
        $project: {
            _id: 0,
            nationality: 1,
            weight_number: 1,
            bmi: {$divide: ["$weight_number", "$height_square"]}
        }
    },
    {
        $group: {
            _id: "$nationality",
            avgBMI: {$avg: "$bmi"},
            minBMI: {$min: "$bmi"},
            maxBMI: {$max: "$bmi"}
        }
    }
]).toArray()