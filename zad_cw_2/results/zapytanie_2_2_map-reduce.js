db.people.mapReduce(
    function () {
        this.credit.forEach(credit => {
            emit(credit.currency, credit.balance)
        })
    },
    function (key, values) {
        return Array.sum(values.map(value => parseFloat(value)))
    },
    {
        out: { inline: 1}
    }
).find()