db.people.insertOne(
    {
        "sex": "Male",
        "first_name": "Piotr",
        "last_name": "Kupczyk",
        "job": "Software Developer",
        "email": "piotr.kupczyk@pjwstk.edu.pl",
        "location": {"city": "Wroclaw", "address": {"streetname": "Some street", "streetnumber": "100"}},
        "description": "vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris",
        "height": "200.00",
        "weight": "100.00",
        "height_number": 200.00,
        "weight_number": 100.00,
        "birth_date": "1997-08-02T02:55:03Z",
        "nationality": "Poland",
        "credit": [{"type": "switch", "number": "6759888939100098699", "currency": "COP", "balance": "5117.06"}]
    }
)