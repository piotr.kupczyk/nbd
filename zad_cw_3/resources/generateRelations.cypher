MATCH path = (origin:Airport)- [:ORIGIN] -(:Flight) - [ :DESTINATION|ORIGIN *..5] -(destination:Airport)
  WHERE origin.name <> destination.name
WITH origin, destination, path, nodes(path) AS nds
UNWIND nds AS nodes
MATCH (nodes:Flight)- [:ASSIGN] -(ticket:Ticket)
WITH origin, destination, path, nodes, min(ticket.price) as theCheapestTicket
WITH origin, destination, path, collect(nodes) as flights , sum(theCheapestTicket) as total
WITH origin, destination, min(total) as totalPrice
CREATE (origin)-[connection:THE_CHEAPEST_CONNECTION {totalPrice: totalPrice}]->(destination)
RETURN origin,
       destination,
       totalPrice;

MATCH path = (_origin: Airport)- [:ORIGIN] -(:Flight) - [ :DESTINATION|ORIGIN *..5] -(_destination: Airport)
  WHERE _origin.name <> _destination.name
WITH _origin, _destination, path, nodes(path) AS nds
UNWIND nds AS nodes
MATCH (nodes:Flight)- [:ASSIGN] -(ticket:Ticket)
WITH _origin, _destination, path, nodes, min(ticket.price) as theCheapestTicket
WITH _origin, _destination, path, collect(nodes) as flights , sum(theCheapestTicket) as total
WITH _origin, _destination, flights, [NODE IN flights WHERE NODE:Flight | ID(NODE)] AS flightIds, total
MATCH (_origin)-[connection:THE_CHEAPEST_CONNECTION]->(_destination)
  WHERE total = connection.totalPrice
SET connection.flightsIds = flightIds
RETURN _origin, _destination, total, connection;