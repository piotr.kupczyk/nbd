MATCH (first: Airport)-[firstConnection:THE_CHEAPEST_CONNECTION]->(second:Airport)-[secondConnection:THE_CHEAPEST_CONNECTION]->(third:Airport)
        WHERE id(first) <> id(second) AND id(first) <> id(third) AND id(second) <> id(third)
    MATCH path = (first: Airport)-[:DESTINATION|ORIGIN *..7]-(second: Airport)-[:DESTINATION|ORIGIN *..7]-(third: Airport)
    WHERE ALL (flight in [node in nodes(path) WHERE node: Flight] WHERE ID(flight) in firstConnection.flightsIds OR ID(flight) in secondConnection.flightsIds)
    RETURN path
    ORDER BY firstConnection.totalPrice + secondConnection.totalPrice
    LIMIT 1;